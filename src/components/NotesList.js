import React, { Component } from 'react'
import axios from 'axios';
import { format } from 'timeago.js';
import { Link } from 'react-router-dom'

export default class NotesList extends Component {
    
    state = {
        notes: []
    }

    async componentDidMount() {
        this.getNotes();
    }

    deleteNote = async (id) => {
        await axios.delete(`http://localhost:4000/api/notes/${id}`);
        this.getNotes();
    }

    async getNotes() {
        const res = await axios.get('http://localhost:4000/api/notes');
        this.setState({notes: res.data});
    }
    
    render() {
        return (
            <div className="row">
                {
                    this.state.notes.map(note => {
                        return (
                            <div className="col-lg-4 p-4" key={note._id}>
                                <div className="card">
                                    <div className="card-header d-flex justify-content-between">
                                        <h4 className="card-title">{note.title}</h4>
                                        <Link className="btn btn-secondary" to={`/edit/${note._id}`}>Edit</Link>
                                    </div>
                                    <div className="card-body">
                                        <p className="card-text">{note.content}</p>
                                        <p className="card-text"><small className="text-muted">{note.author}</small> - <small className="text-muted">{format(note.date)}</small></p>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-danger" 
                                                onClick={() => this.deleteNote(note._id)}>Delete</button>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}
